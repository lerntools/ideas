# Idémyldring

Instruks for forfattere{.subtitle}

En «idémyldringsøkt» brukes for å samle idéer om et gitt emne. Det består av et antall kort, der hvert av dem kan inneholder flere deler av innholdet. Innholdet kan være tekst, et bilde, eller faktisk også en lenke.

![](text/nb/ideas/example1.png){.center}

Flere personer kan samarbeide i en idémyldring, og endringene synkroniseres automatisk og (nesten) uten forsinkelse. Dette krever dog en moderne nettleser (f.eks. Firefox, Vivaldi, eller Chromium) med støtte for vevsockets.

## Forfatter

### Opprettelse av idémyldringsøkt

Kun én person registrert på nettsiden med tilstrekkelige rettigheter kan opprette en idémyldringsøkt som *forfatter*:

1. Logg inn på nettsiden (viktig, fordi ellers vil samlingen av idéer vises i deltagerens visning)
2. Gå til samlingen av idéer
3. Klikk på det blå pluss-symbolet

### Innstillinger for en idémyldringsøkt

![](text/nb/ideas/example2.png){.center}

1. Grunnleggende innstillinger

	- Navn: Kort beskrivelse av idésamlingen, vist som navnet på siden for deltagere.
	- Beskrivelse: Kort forklaring, vist som andre linje i navnet.
	- Billettnummer: Streng som lar andre *deltagere* få tilgang til idésamlingen.

2. Deltagertilganger for innhold …:

	- Tillegging: Rettighet til å opprette nytt innhold for eksisterende kort.
	- Endring: Rettighet til å endre eksisterende innhold for kort.
	- Sletting: Rettighet til å slette eksisterende innhold (også nyttig for flytting).

3. Deltagertilganger for kort …:

	- Tillegging: Rettighet til å legge til ny kort.
	- Navneendring: Rettighet til å endre navn (og farge) for eksisterende kort.
	- Sletting: Rettighet til å slette eksisterende kort.

4. Flere innstillinger

	- Deltagere kan laste opp filer (opplasting): Skrur på opplasting av filer
	- Presentasjon av kort som lysbildevisning: Alternativt kan inneholdet i et kort presenteres som lysbildevisning.

«Lagre» godtar innstillingene, og nylig opprettet idésamling vises nå i oversikten.

### Oversikt over idémyldringsøkter

Idémyldringsøktene presenteres i tabellform i oversikten:

![](text/nb/ideas/example3.png){.center}

- Visning (triangel): Viser idésamlingen som forfatteren i praksis eier alle rettighetene til.
- Redigering (blyantsymbol): Endre innstillingene for idémyldringsøkt, f.eks. en fullført økt kan låses helt og brukes kun som informasjonstavle
- Deling (deling): Viser billettnummeret og en QR-kode for deltagerne
- Duplisering (firkant): Dupliserer en eksisterende idésamling
- Eksportering (pil): Eksporterer samlingen i markdown eller HTML-firmat. Hvis du ønsker å redigere samlingen ytterligere, f.eks. med LibreOffice, er dette enkelt mulig via HTML-formatet.
- Sletting (papirkurv): Slett idésamlingen med alle kort og deres innhold. Dette kan ikke angres.

### Eksportering og importering

Eksport til følgende formater er mulig:

- JSON: Maskinlesbart format, egnet for senere import.
- JSON og opplastinger — for senere import, inklusive opplastede filer (ingen garanti).
- HTML: Maskinlesbart format, som kan åpnes i nettlesere eller tekstbehandlere.

Import er kun mulig i JSON-format. For JSON og opplastinger _bør_ de være inkludert i opplastingen, men det er ingen garanti for det. Av sikkerhetshensyn bør de lagres for seg selv.

### Tips

- Som forfatter kan du låse individuelle kort. Dette er mulig etter å ha vis (vist ovenfor) en idésamling ved å klikke på kortets overskrift.
- Skjuling av kort er også mulig, Merk deg at dataene fremdeles er tilgjengelige for teknisk kyndige brukere, dvs. kun visningen er fjernet.
- Kortenes rekkefølge er bestemt av deres opprettelse. Det kan dog defineres sortering etterpå (ved å klikke på kortets overskrift).
- Innholdet kan flyttes fra ett kort til et annet med dra og slipp via bindeleddsmenyen.

##	Deltagere

### Deltagelse i en idémyldringsøkt

Det finnes to måter å ta del i en idémyldringsøkt:

1. Bruk QR-koden, som forfatteren viser med funksjonen «Del»
2. Finn idémyldringsøkten via navigasjonen og skriv inn billettnummeret

### Arbeid med idémyldringsøkten

Redigeringsmulighetene avhenger av rettigheter, som forfatteren har definert. En kort hjelpetekst om de mulige handlingene er å finne ved å trykke på «Hjelp»-knappen.
