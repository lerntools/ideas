# Session de brainstorming

Instructions pour les auteur·es{.subtitle}

Une «session de brainstorming » sert à faire un brainstorming collectif sur un thème donné. Elle se compose d'un certain nombre de cartes, chacune pouvant contenir plusieurs contenus. Un contenu peut être un texte, une image ou même un lien.

![](text/fr/ideas/example1.png){.center}

Plusieurs personnes peuvent travailler *simultanément* sur une session de brainstorming, les modifications sont alors synchronisées automatiquement et (presque) sans délai. Pour cela, il faut toutefois disposer d'un navigateur web actuel (p. ex. Firefox, Safari, Chrome) prenant en charge les websockets.

## auteur·e

### Création d'une session de brainstorming

Seule une personne enregistrée sur le site et disposant des droits nécessaires peut créer une nouvelle session de brainstorming en tant qu' *auteur·e* :

1. Se connecter au site web (important, sinon la collecte d'idées apparaît dans la vue des participant·es)
2. Navigation vers le point session de brainstorming
3. Cliquer sur le symbole plus bleu

### Paramètres d'une session de brainstorming

![](text/fr/ideas/example2.png){.center}

1. Options basiques

	- Titre : brève description de la session de brainstorming, apparaît comme titre de la page pour les participant·es.
	- Description : brève explication, apparaît comme deuxième ligne dans le titre.
	- Ticket : chaîne de caractères permettant à d'autres *participant·es* d'accéder à la collection d'idées.

2. Permission d’accès au contenu... :

	- ajouter des informations : le droit d'écrire de nouveaux contenus sur des cartes existantes.
	- modifier une fiche : le droit de modifier le contenu existant sur une carte.
	- supprimer des cartes : le droit de supprimer des contenus existants (également nécessaire pour les déplacements).

3. Permission d’accès aux cartes... :

	- ajouter des cartes : le droit d'ajouter de nouvelles cartes.
	- changer le nom d'une carte : le droit de modifier le titre (et la couleur) d'une carte existante.
	- supprimer une carte : le droit de supprimer une carte existante.

4. Plus de paramètres

	- Les participant·es peuvent télécharger des fichiers (upload) : permet de télécharger des fichiers.
	- Présentation de cartes sous forme de diaporama : en option, le contenu d'une carte peut être présenté sous forme de diaporama.

« Enregistrer » reprend les paramètres, la session de brainstorming nouvellement créée apparaît maintenant dans l'aperçu.

### Aperçu des sessions de brainstorming

Les sessions de brainstormings ont présentées sous forme de tableau dans l'aperçu :

![](text/fr/ideas/example3.png){.center}

- Afficher (triangle) : Afficher la session de brainstorming, l'auteur·e possédant en principe tous les droits.
- Modifier (icône en forme de crayon) : Modifier les paramètres de la session de brainstorming, par ex. une collection terminée peut ensuite être complètement bloquée et utilisée comme simple panneau d'information.
- Partager (Share) : Affichage du ticket et d'un code QR pour les participant·es.
- Dupliquer (carrés) : Dupliquer une collection d'idées existante.
- Exporter (flèche) : Exporter la collection au format Markdown ou HTML. Si tu souhaites continuer à travailler avec la session de brainstorming, par exemple avec LibreOffice, cela est tout à fait possible avec le format HTML.
- Supprimer (poubelle) : Suppression de la session de brainstorming avec toutes les cartes et tous les contenus. Cette étape ne peut pas être annulée.

### Exportation et importation

L'exportation est possible dans les formats suivants :

- JSON : format lisible par machine, convient pour une importation ultérieure
- JSON & Uploads : pour l'importation ultérieure, y compris les fichiers téléchargés (sans garantie)
- HTML : format lisible par les humains, peut être ouvert dans un navigateur ou un système de traitement de texte.

L'importation n'est possible qu'au format JSON. Pour les JSON & uploads, ils _devraient_ être inclus dans l'upload, mais il n'y a aucune garantie. Par sécurité, ils devraient être sauvegardés séparément.

### Conseils

- En tant qu'auteur·e, il est possible de bloquer certaines cartes. Cela est possible après l'affichage (voir ci-dessus) d'une session de brainstorming en cliquant sur l'en-tête de la carte.
- Il est également possible de cacher des cartes. Merci de noter que les données sont tout de même disponibles pour les utilisateurs·trices ayant des connaissances techniques, c'est-à-dire que seul l'affichage est supprimé.
- L'ordre des cartes dépend en principe de leur création. Il est toutefois possible de définir un ordre de tri a posteriori (également en cliquant sur l'en-tête d'une carte).
- Les contenus peuvent être déplacés d'une carte à l'autre par glisser-déposer ou via le menu contextuel.

## participant·es

### Participer à une session de brainstorming

Pour participer à une session de brainstorming, il y a deux possibilités :

1. utilisation du code QR affiché par l'auteur·e avec la fonction Partager
2. appel du point session de brainstorming dans la navigation et saisie du ticket

### Travailler avec la session de brainstorming

Les possibilités d'édition dépendent des droits définis par l'auteur·e. Une brève aide sur les actions possibles peut être affichée en cliquant sur le bouton « Aide ».
