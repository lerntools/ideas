import Icon from './img/ideas.svg';
const meta = require('./locales/Index.json');
let infos = {'title': {}, 'subtitle': {}}
for (const lang in meta) {
	if ('title' in meta[lang]) {
		infos['title'][lang] = meta[lang]['title'];
	} else {
		infos['title'][lang] = "";
	}
	if ('subtitle' in meta[lang]) {
		infos['subtitle'][lang] = meta[lang]['subtitle'];
	} else {
		infos['subtitle'][lang] = "";
	}
}

export default {
	id: "ideas",
	meta: {
		title: infos['title'],
		text: infos['subtitle'],
		to: "ideas-index",
		adminto: "ideas-admin",
		icon: Icon,
		role: "ideas",
		index: true
	},
	routes: [
		{	path: '/ideas-index', name:'ideas-index', component: () => import('./views/Index.vue') },
		{	path: '/ideas', name:'ideas-login', component: () => import('./views/Login.vue') },
		{	path: '/ideas/:ticket', name:'ideas-part', component: () => import('./views/Part.vue') },
		{	path: '/ideas-edit', name:'ideas-edit', component: () => import('./views/Edit.vue') },
		{	path: '/ideas-slideshow/:ticket/:cardId', name:'ideas-slideshow', component: () => import('./views/Slideshow.vue') },
		{	path: '/ideas-admin', name:'ideas-admin', component: () => import('./views/Admin.vue') },
	],
	uploadMaxSize: 5*1024*1024,
	imageMaxSize: 0.5*1024*1024
}
