//External libs
const { check,body,param,oneOf,validationResult }=require('express-validator/check');
const { sanitize }=require('express-validator/filter');
const config=__config;
const consts=require ('../../../consts.js');
const debug=require('debug')('ideas');
const cron=require('node-cron');
const generator=require('../../../main/server/passwordHelper');
const path=require('path');
const WebSocket=require('ws');
const mongoose=require('mongoose');

//Internal libs
const mainController=require('../../../main/server/controller');
const checkUserRole=require('../../../main/server/userHelper').checkUserRole;
const sendInfoMail=require('../../../main/server/smtpHelper').sendInfoMail;
const uploadHelper=require('../../../main/server/uploadHelper');

//database
const Ideas=require('./modelIdeas.js');
const IdeasCard=require('./modelIdeasCard.js');

//constants
const IDEAS_RIGHTS=["addCard", "editCard", "delCard", "addContent", "editContent", "delContent", "upload"];
const MAX_CARDS=50;
const MAX_CONTENTS=50;
const TICKET_LENGTH=10;
const WS_PING_INTERVAL=100;

/********************************************************************************
 Common functions
 ********************************************************************************/

 /*
  Check authorization
  - used in router
  */
 exports.checkAuth=function(req, res, next) {
 	mainController.checkAuthAndRole(req,res,next,"ideas");
 }


/********************************************************************************
 Author
 ********************************************************************************/

/*
 Get meta information of all collections
 - Authenticated with checkAuth (route)
 - Autorised by DB
 */
exports.getIdeasMetas=function(req, res, next) {
	Ideas.find({owner:res.locals.user.login}, 'title ts desc owner ticket created info slideshow upload addContent editContent delContent addCard editCard delCard').sort({ title: 'asc'}).exec(function(err,allIdeas) {
		if (err) debug(err);
		if (!allIdeas) return res.json({});
		res.json(allIdeas);
	});
}

/*
 Get meta information for one collection
 - Authenticated with checkAuth (route)
 - Autorised by DB
 */
exports.getIdeasMeta=[
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		Ideas.findOne({_id:req.params.id, owner:res.locals.user.login}, 'title desc owner ticket created info slideshow upload addContent editContent delContent addCard editCard delCard cards').sort({ title: 'asc'}).exec(function(err,ideas) {
			//even cards is specified in query, no contents will be transfered, as cards is not populated
			if (err) debug(err);
			if (!ideas) return res.json({});
			res.json(ideas);
		})
	}
]

/*
 Import Json
 - Authenticated with checkAuth (route)
 - No further authorization
 */
exports.addNewIdeas=async function(req,res,next) {
	var data=req.body.ideas;
	//Cancel if file is missing
	if (!data || !data.title) return res.json([{msg:'no-file-selected'}]);
	//Validate meta-data
	var errors=[];
	var ideas=new Ideas ({
		title:		(data.title && data.title.match(consts.REGEX_TEXT_300)) ? data.title : '???',
		desc:		(data.desc && data.desc.match(consts.REGEX_TEXT_AREA_500)) ? data.desc : '',
		owner:		res.locals.user.login,
		ticket:		(data.ticket && data.ticket.match(consts.REGEX_TICKET)) ? data.ticket : generator.generate({length: TICKET_LENGTH, numbers: true}),
		created:	(data.created>0) ? data.created : 0,
		info:		data.info==true,
		slideshow:	data.slideshow==true,
		cards:		[]
	});
	//access rights
	for (var right of IDEAS_RIGHTS) {
		ideas[right]=data[right] ? true : false;
		if (data.access && data.access.indexOf(right)>=0) ideas[right]=true; //import legacy format
	}
	//Cards
	if (data.cards) {
		for (dc of data.cards) {
			var card=new IdeasCard({
				title: 	(dc.title && dc.title.match(consts.REGEX_TEXT_300)) ? dc.title : 'card',
				sort:	(dc.sort>0) ? dc.sort : 0,
				color:	(dc.color && dc.color.match(consts.REGEX_COLOR)) ? dc.color : '#808080',
				locked: dc.locked==true,
				hidden: dc.hidden==true,
				contents: [ ]
			});
			//Contents
			if (dc.contents) {
				for (c of dc.contents) {
					var content={
						type:	(c.type && c.type.match(/a|text|upload/))	? c.type : 'text',
						text:	(c.text && c.text.match(consts.REGEX_TEXT_AREA) && c.text.length<=500) ? c.text : '???',
						url:	(c.url && c.text.match(consts.REGEX_TEXT)) ? c.url : ''
					}
					//text
					if (content.type=='text') {
						content.url='';
					}
					//hyperlink
					if (content.type=='a' && (!content.url.match(consts.REGEX_URL) || content.url.length>300)) {
						content.url='';
						content.type='text';
					}
					//uploads
					if (content.type=='upload') {
						if (!content.url) content.type="text";
						//upload includes as base64-data?
					 	else if (content.url.startsWith('data:')) {
							var uploadName=uploadHelper.addBase64Upload(content.url,'ideas');
							content.url=uploadName;
							if (!uploadName) content.type='text';
						}
						//upload realtive path -check if corresponding file exists
						else {
							if (!uploadHelper.uploadExists(content.url,'ideas')) {
								content.url='';
								content.type='text';
							}
						}
					}
					card.contents.push(content);
				}
			}
			ideas.cards.push(card);
		}
	}
	//Save cards
	for (var card of ideas.cards) {
		try {
			await card.save();
		}
		catch (err) {
			debug(err);
			errors.push({msg:'error-import-card',params:{title:card.title}});
		}
	}
	//Ensure unique ticket
	Ideas.findOne({ticket:ideas.ticket}, function(err,ideasWithSameTicket) {
		if (err) debug(err);
		if (ideasWithSameTicket) ideas.ticket=generator.generate({length: TICKET_LENGTH, numbers: true});
		//Save collection itself
		ideas.save(function(err) {
			if (err) {
				debug(err);
				errors.push({msg:'error-import-db'});
			}
			return res.json(errors);
		})
	})
}

/*
 Delete ideas collection
 - Authenticated with checkAuth(route)
 - Authorization DB request (session.login)
 */
exports.deleteIdeas=[
	sanitize('id').trim().escape(),
	async (req, res, next)=> {
		var id=req.params.id;
		var ideas=await Ideas.findById(id);
		if (ideas && (ideas.owner==res.locals.user.login || checkUserRole(res,'admin') )) {
			//Delete all cards
			for (var card of ideas.cards) {
				try {
					await IdeasCard.deleteOne({_id:card._id});
				}
				catch (err) {
					debug(err);
					return res.json([{msg: 'unknown-error'}]);
				}
			}
			//Delete ideas collection itself
			Ideas.deleteOne({_id:id}, function(err) {
				if (err) debug(err);
				ideasSockets[ideas.ticket]=new Set();
				return res.json({});
			})
		}
		else {
			return res.json([{msg: 'not-allowed'}]);
		}
	}
]

/*
 get ideas collection
 - Authenticated with checkAuth(route)
 - Authorization DB request (login)
 */
exports.getIdeasById=[
   sanitize('id').trim().escape(),
   (req, res, next)=> {
	   var id=req.params.id;
	   Ideas.findOne({_id:id, owner:res.locals.user.login }).populate('cards').exec( function(err,ideas) {
		   if (err || !ideas) return res.json([{msg:'not-allowed'}]);
		   return res.json(ideas);
	   })
   }
]

/*
 Save changes to an existing ideas collection or create a new collection
 - Authenticated with checkAuth(route)
 - Authorization: stored id in the session
 */
exports.setIdeasMeta=[
	body('title').matches(consts.REGEX_TEXT_300),
	body('ticket').matches(consts.REGEX_TICKET),
	body('desc').matches(consts.REGEX_TEXT_AREA_500),
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//Check if ticket is already taken
		var ticket=req.body.ticket;
		var id=req.params.id;
		Ideas.findOne({ticket:ticket}, function(err,ideas) {
			if (err) debug(err);
			if (ideas && ideas.id.toString()!=id) return res.json([{msg:'ticket-taken'}]);
			//Save settings
			var meta={
				title:req.body.title,
				desc:req.body.desc,
				upload:req.body.upload,
				info:req.body.info,
				slideshow:req.body.slideshow,
				addCard:req.body.addCard,
				editCard:req.body.editCard,
				delCard:req.body.delCard,
				addContent:req.body.addContent,
				editContent:req.body.editContent,
				delContent:req.body.delContent,
				ticket:req.body.ticket,
			};
			//update existing collection
			if (id) {
				Ideas.updateOne({_id:id, owner:res.locals.user.login}, { $set: meta },	function(err) {
					if (err) debug(err);
					broadcastWS(ticket,{type:'metaChanged',meta:meta});
					return res.json({});
				})
			}
			//create new chat
			else {
				meta.owner=res.locals.user.login;
				var ideas=new Ideas(meta);
				ideas.save(function(err) {
					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
					broadcastWS(ticket,{type:'metaChanged',meta:meta});
					return res.json({})
				})
			}
		})
	}
]



/********************************************************************************
 Admin
 ********************************************************************************/

/*
 index for author
 Authorization router
 */
exports.getIdeasMetaAdmin=function(req,res,next) {
	Ideas.find().sort({ owner:'asc', title: 'asc'}).exec(function(err,allIdeas) {
		if (err) debug(err);
		if (!allIdeas) res.json({})
		res.json(allIdeas);
	});
}

/********************************************************************************
 Participants
 ********************************************************************************/

 //initially check ticket and availability, used in routes
 exports.loadIdeasFromTicket=[
  	check('ticket').matches(consts.REGEX_TICKET),
  	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
        //try to load chat from db
        var ticket=req.params.ticket || req.body.ticket || req.query.ticket;
		if (!ticket) return res.json([{msg:'validation-error'}]);
        Ideas.findOne({ticket:ticket}, function(err, ideas) {
  			if (err) debug(err);
  			if (!ideas) return res.status(404).json([{msg:'ticket-invalid'}]);
            res.locals.ideas=ideas;
			res.locals.ticket=ticket;
 		    next();
        })
 	}
 ]

 // Load card by cardid in url,  depends on loadIdeasFromTicket, used in router
exports.checkUrlCardid=[
	sanitize('cardid').trim().escape(),
 	(req, res, next)=> {
		var cardid=req.params.cardid || req.body.cardid || req.query.cardid;
 		//Allow edit only, if card matches ticket (cardid isn't random in mongoose)
 		if (res.locals.ideas.cards.indexOf(cardid)>=0) {
 			res.locals.cardid=cardid;
 			next();
 		}
 		else {
 			return res.status(404).json([{msg:'not-available'}])
 		}
 	}
 ]

 //return socket url
 exports.getSocketUrl=function(req,res,next) {
 	var url=(config.URL_PROTO=='http://' ? 'ws://' : 'wss://')+path.join(config.URL_HOST,config.URL_PREFIX,'ideas','ws',res.locals.ideas.ticket);
 	res.json({url:url});
 }

//get ideas collection with cards - complete
exports.getPopulatedIdeasByTicket=[
	sanitize('ticket').trim().escape(),
    (req, res, next)=> {
		var ticket=req.params.ticket;
 	   	Ideas.findOne({ticket:ticket}).populate('cards').exec( function(err,ideas) {
 		   	if (err || !ideas) return res.status(404).json([{msg:'invalid-ticket'}]);
 		   	return res.json(ideas);
 	   	})
    }
 ]

 /*
  Save single content
  - Authorization loadIdeasFromTicket (route), checkUrlCardId (route) and isAllowedTo
  - Limit maximum number of entries
 */
 exports.saveContent=[
 	//body('text').matches(consts.REGEX_TEXT_AREA_1000),
 	body('type').matches("^(text|a|upload)$"),
 	sanitize('cardid').trim().escape(),
	sanitize('contentid').trim().escape(),
 	sanitize('pos').trim().escape(),
 	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
 		var type=req.body.type;
 		var text=req.body.text;
 		var url=req.body.url;
    if(!text) {
      if(type) {
        text = " ";
      }
      else {
        text =" ";
      }
    }
 		var pos=Number(req.body.pos);
 		if (isNaN(pos) || pos<0 ) pos=1000; //evtl. hier
		if (type=="a" && (text =="" || text)) {
			if (url=="" ) return res.json([{msg:'url-missing'}]);
			if (url.length>1000) return res.json([{msg:'url-invalid'}]);
			if (!isValidHttpUrl(url)) return res.json([{msg:'url-invalid'}]);
		}
		else if (type=="upload" && (text =="" || text)) {
			if (!config.UPLOAD_DIR) return res.json([{msg:'upload-forbidden'}]);
			if (url=="" ) return res.json([{msg:'validation-error'}]);
			if (url.length>consts.MAX_UPLOAD_SIZE*1.4) return res.json([{msg:'filessize-exceeded'}]);
			if (!isAllowedTo(req,res,res.locals.ideas,"upload",null)) return res.json ([{msg:'forbidden'}]);
			//save file to upload dir when new file is submitted
			if (url.startsWith('data:')) {
				var uploadName=uploadHelper.addBase64Upload(url,'ideas');
				if (!uploadName) return res.json([{msg:'upload-failed'}]);
				url=uploadName;
			}
		}
		if (text=="" && type=="text") return res.json([{msg:'validation-error'}]);
 		var contentid=req.params.contentid;
		var cardid=req.params.cardid;
 		IdeasCard.findById(cardid, function (err,card) {
			//Exit if no card is found
			if (!card || err) return res.json([{msg:'already-deleted'}]);
 			//Check maximum number of contents
 			if (card.contents.length>=MAX_CONTENTS) return res.json([{msg:'max-number-reached'}]);
 			//Add new content
 			if ((!contentid || contentid==0) && isAllowedTo(req,res,res.locals.ideas,"addContent",card)) {
				contentid=mongoose.Types.ObjectId();
 				IdeasCard.updateOne({_id:cardid}, { $push: {contents: {$each: [ {_id:contentid, type:type, text:text, url:url} ], $position:pos}}}, function(err) {
					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
 					broadcastWS(res.locals.ticket, {type:'newContent', cardid:cardid, content: {_id: contentid, type:type, text:text, url:url, pos:pos }});
 					res.json({});
 				})
 			}
 			//Update content
 			else if (isAllowedTo(req,res,res.locals.ideas,"editContent",card)) {
 				IdeasCard.update({"_id":cardid, "contents._id":contentid}, { $set: { "contents.$.type": type, "contents.$.text": text, "contents.$.url":url }}, function(err) {
 					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
					broadcastWS(res.locals.ticket, {type: 'updateContent',cardid:cardid, content: {_id:contentid, type:type, text:text, url:url }});
 					res.json({});
 				})
 			}
 			else return res.json([{msg:'not-allowed'}])
 		})
 	}
 ]

 // Delete content, aAuthorization checkTicket (route)
exports.delContent=[
 	sanitize('contentid').trim().escape(),
 	(req, res, next)=> {
		var contentid=req.params.contentid;
		var cardid=res.locals.cardid;
 		IdeasCard.findById(cardid, function (err,card) {
 			if (err) debug(err);
 			if (!isAllowedTo(req,res,res.locals.ideas,"delContent",card)) return res.json([{msg:'not-allowed'}]);
 			IdeasCard.findById(res.locals.cardid, function(err, card) {
 				if (err) debug(err);
 				if (!card) return res.json([{msg:'already-deleted'}]);
 				IdeasCard.updateOne({_id:cardid}, {  $pull: {contents: { _id : contentid }}}, function(err) {
					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
					broadcastWS(res.locals.ticket, {type: 'delContent',cardid:cardid, contentid:contentid });
					return res.json({});
 				})
 			})
 		})
 	}
]

/**
 * Get an ideas card 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getCard=function(req, res, next) {
	var cardid=res.locals.cardid;
	IdeasCard.findById(cardid, function (err,card) {
		if (err) debug(err);
		if (!card) return res.status(404).json([{msg:'not-available'}]);
		return res.status(200).json(card);
	})
}

/*
 Save card settings (and create new if required)
 - Authorization loadIdeasFromTicket (route) and isAllowedTo
 */
exports.saveCard=[
	body('title').matches(consts.REGEX_TEXT_AREA_300),
	body('sort').isInt({min:0}),
	body('color').matches(consts.REGEX_COLOR),
	body('locked').isBoolean(),
	body('hidden').isBoolean(),
	sanitize('cardid').trim().escape(),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var title=req.body.title;
		var sort=req.body.sort;
		var color=req.body.color;
		var cardid=req.params.cardid;
		var locked=req.body.locked && isOwner(req,res, res.locals.ideas); //Only author is allowed to lock a card
		var hidden=req.body.hidden && isOwner(req,res, res.locals.ideas); //Only author is allowed to hide a card
		//add new card
		if (!cardid || cardid==0){
			if (!isAllowedTo(req,res,res.locals.ideas,"addCard",null)) return res.json([{msg:'not-allowed'}]);
			if (res.locals.ideas.cards.length>MAX_CARDS) return res.json([{msg:'max-number-reached'}]);
			cardid=mongoose.Types.ObjectId();
			var card=new IdeasCard({ _id:cardid,title:req.body.title, sort:sort, color:color, locked:locked, hidden:hidden, contents: []});
			Ideas.updateOne({ticket:res.locals.ticket}, { $push: { cards: card } }, function(err) {
				if (err) debug(err);
				card.save(function (err) {
					if (err) debug(err);
					broadcastWS(res.locals.ticket, {type: 'addCard',cardid:cardid, card:card });
					res.json({});
				})
			})
		}
		//update existing card
		else {
			IdeasCard.findById(cardid, function (err,card) {
				if (err) debug(err);
				//Abort if not allowed to edit card
				if (!card || !isAllowedTo(req,res,res.locals.ideas,"editCard",card)) return res.json([{msg:'not-allowed'}]);
				IdeasCard.updateOne({_id:cardid}, { $set: { title: title, sort:sort, color:color, locked:locked, hidden:hidden }}, function(err) {
					if (err) debug(err);
					card.title=title; card.sort=sort; card.color=color; card.locked=locked; card.hidden=hidden;
					broadcastWS(res.locals.ticket, {type: 'updateCard', card:card });
					res.json({});
				})
			})
		}
	}
]

/*
 Delete existing card
 - Authorization  loadIdeasFromTicket (route) and isAllowedTo
 */
exports.delCard=function(req, res, next) {
	var cardid=res.locals.cardid;
	IdeasCard.findById(cardid, function (err,card) {
		if (err) debug(err);
		if (!card || !isAllowedTo(req,res,res.locals.ideas,'delCard',card)) return res.json([{msg:'not-allowed'}]);
		IdeasCard.deleteOne({_id:cardid}, function(err) {
			if (err) debug(err);
			Ideas.updateOne({ticket:res.locals.ticket}, {$pull: {cards: cardid}, $set: {ts: currentTime()}}, function(err) {
				if (err) debug(err);
				broadcastWS(res.locals.ticket,{type:'delCard', cardid:cardid});
				res.json({});
			})
		})
	})
}


/********************************************************************************
 Websocket
 ********************************************************************************/

//List of all ws-clients
var ideasSockets={};

/*
 Register new client on ws-connection
*/
exports.initWs=function(ws, req) {
	var ticket=req.params.ticket;
	Ideas.findOne({ticket:ticket}, function(err, ideas) {
		if (err) debug(err);
		if (ideas) {
			if (!ideasSockets[ticket]) ideasSockets[ticket]=new Set();
			ideasSockets[ticket].add(ws);
			ws.on('pong', function() {
				this.isAlive=true;
			});
			debug("New WS ideas client with Ticket "+ticket);
		}
		else {
			debug('WS-request dropped due to invalid ticket.');
		}
	})
}

/*
 Broadcast info on changes (for specific ticket)
 */
function broadcastWS(ticket, message) {
	var socketSet=ideasSockets[ticket];
	var ts=currentTime();
	message.ts=ts;
	if (!socketSet) return;
	var json=JSON.stringify(message);
	socketSet.forEach(function(socket) {
		if (socket.readyState==WebSocket.OPEN) {
			try {socket.send(json);	} catch (e) {debug(e);}
		}
	});
	//update timestamp
	Ideas.updateOne({ticket:ticket}, {$set: {ts:ts}}, function (err) {
		if (err) debug(err);
	});
}

/*
 Ping all websockets in order to
 - check the connection
 - renew connection (ensure the reverse proxy won't terminate the connection
 */
setInterval(function () {
	var count=0;
	for (ticket in ideasSockets) {
		socketSet=ideasSockets[ticket];
		socketSet.forEach(function(socket) {
			if (socket.isAlive===false || socket.readyState!=WebSocket.OPEN) {
				debug("WS ideas client closed for ticket: "+ticket);
				socketSet.delete(socket);
				try {socket.close();} catch (e) {debug(e);}
			}
			else {
				count++;
				socket.isAlive=false;
				try { socket.ping();} catch (e) {debug (e);}
			}
		})
	}
	debug("Number of open WS sockets: "+count)
}, WS_PING_INTERVAL*1000);


/********************************************************************************
 Utils
 ********************************************************************************/

/*
 check if text is valid http(s) url, see alos Part.vue
*/
function isValidHttpUrl(text) {
	try {
		const url = new URL(text)
		return url.protocol === 'http:' || url.protocol === 'https:'
	}
	catch(e) { return false }
}

/*
 Check for speficic authorization
 */
function isAllowedTo(req, res, ideas,right, card ) {
	result=isOwner(req,res, ideas) ||	//Owner is generally authorized
		(ideas && ideas[right] && (card==null || ( !card.locked && !card.hidden)));
	return result;
}

/*
 Check if authenticated user is owner of the collection
 */
function isOwner(req, res, ideas) {
	var result=res && ideas && res.locals.user && res.locals.user.login==ideas.owner;
	return result;
}

/*
 Current time in milliseconds
 */
function currentTime() {
	return (new Date()).getTime();
}

/*
 Check all collections for changes once a day.
 */
if (config.INFO_MAIL_APPS) cron.schedule("01 01 * * *", cronInfoAuthor);
function cronInfoAuthor() {
	debug("Checking all collections for changes");
	var oneDay=24*3600*1000;
	var now=currentTime();
	Ideas.find({info:true}).populate('cards').exec( function(err,infoIdeas) {
		if (err) debug(err);
		if (!infoIdeas) exit;
		for (ideas of infoIdeas) {
			if (now-ideas.ts<oneDay) {
				//TODO - use lng without lot of packaes in express
				var url=config.URL_PROTO+path.join(config.URL_HOST,config.URL_PREFIX,'app','#',"ideas",ideas.ticket);
				var html="<p>Brainstorming \""+ideas.title+"\" has new or updated entries. Click on the following link to display them:</p>"+
					"<a href='"+url+"'>"+url+"</a></p>"+
					"<p>Note: You can disable notifications in the settings of you brainstorming session.</p>";
				var subject="Brainstorming \""+ideas.title+"\"";
				sendInfoMail(ideas.owner,subject,html)
				debug("Changes in "+ideas.title);
			}
			else {
				debug("No changes in "+ideas.title)
			}
		}
	})
}

/*
 Clean up orphaned uploads
*/
if (config.UPLOAD_DIR) cron.schedule("02 02 * * *", cleanOrphanedUploads);
if (config.UPLOAD_DIR) cleanOrphanedUploads();	//also clean on server start
function cleanOrphanedUploads() {
	debug("Checking for orphaned uploads");
	IdeasCard.find().exec( function(err,cards) {
		var uploadNames=[];
		if (!cards) return;
		for (var card of cards) {
			for (var content of card.contents) {
				if (content.type=='upload' && content.url) uploadNames.push(content.url);
			}
		}
		uploadHelper.cleanOrphanedUploads(uploadNames,'ideas');
	})
}

/*
Delete all user data - only used from main controller
*/
exports.deleteUserData=function(login) {
	return new Promise(async(resolve)=> {
		var ideasToDelete=await Ideas.find({owner:login});
		for (ideas of ideasToDelete) {
			//delete cards
			for (card of ideas.cards) {
				await IdeasCard.deleteOne({_id:card._id});
			}
			//delete collection itself
			await Ideas.deleteOne({_id:ideas._id});
		}
		resolve();
	})
}

