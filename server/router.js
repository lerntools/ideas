var express=require('express');
var router=express.Router();
var controller=require('./controller');
var mainController=require('../../../main/server/controller');


//author routes
router.get('/ideas-meta', controller.checkAuth, controller.getIdeasMetas);
router.get('/ideas-meta/:id', controller.checkAuth, controller.getIdeasMeta);
router.put('/ideas-meta/:id', controller.checkAuth, controller.setIdeasMeta);
router.post('/ideas-meta', controller.checkAuth, controller. setIdeasMeta);
router.get('/ideas/:id', controller.checkAuth, controller.getIdeasById);
router.post('/ideas',controller.checkAuth, controller.addNewIdeas);
router.delete('/ideas/:id', controller.checkAuth, controller.deleteIdeas);

//admin routes
router.get('/admin/ideas-meta', mainController.checkAdmin, controller.getIdeasMetaAdmin)
router.delete('/admin/ideas/:id',mainController.checkAdmin, controller.deleteIdeas);

//participants routes
router.get('/part/socket/:ticket', controller.loadIdeasFromTicket, controller.getSocketUrl);
router.get('/part/ideas/:ticket', controller.getPopulatedIdeasByTicket);	//checkTicket not necessary here

router.put('/part/ideas/:ticket/:cardid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.saveCard);
router.get('/part/ideas/:ticket/:cardid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.getCard);
router.post('/part/ideas/:ticket',controller.loadIdeasFromTicket,controller.saveCard);
router.put('/part/ideas/:ticket/:cardid/:contentid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.saveContent);
router.post('/part/ideas/:ticket/:cardid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.saveContent);
router.delete('/part/ideas/:ticket/:cardid/:contentid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.delContent);
router.delete('/part/ideas/:ticket/:cardid', controller.loadIdeasFromTicket, controller.checkUrlCardid, controller.delCard);

//Websocket to signal changes
router.ws('/ws/:ticket', controller.initWs);

module.exports=router;
