var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var IdeasSchema=new Schema({
	title :		{type: String, required: true, max: 300},
	desc :		{type: String, required: true, max: 500},
	owner:		{type: String, max: 100},
	ticket:		{type: String, unique: true, max: 100},
	created:	{type: Number, required: true, default: 0},
	ts:			{type: Number, required: true, default: 0},
	info:		{type: Boolean, default: false},
	slideshow:	{type: Boolean, default: false},
	upload:		{type: Boolean, default: false},
	addContent:	{type: Boolean, default: false},
	editContent:{type: Boolean, default: false},
	delContent: {type: Boolean, default: false},
	addCard:	{type: Boolean, default: false},
	editCard:	{type: Boolean, default: false},
	delCard:	{type: Boolean, default: false},
	cards:		[{ type: Schema.Types.ObjectId, ref:'IdeasCard' }]
});

module.exports=mongoose.model('Ideas', IdeasSchema );
