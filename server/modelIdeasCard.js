var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var IdeasCardContentSchema=new Schema({
	type: 	{type: String, required: true, default: "text", max: 10},
	text: 	{type: String, required: true, max: 1000},
	url:  	{type: String, required: false}
})

var IdeasCardSchema=new Schema({
	title: 	{type: String, required: true, max: 300},
	sort:	{type: Number, required: true, default: 0},
	color: 	{type: String, default: "#6c757d"},
	locked: {type: Boolean, default: false},
	hidden: {type: Boolean, default: false},
	contents: [ IdeasCardContentSchema ]
});

module.exports=mongoose.model('IdeasCard', IdeasCardSchema );
